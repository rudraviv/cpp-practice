//Demonstration of Friend function
//Swapping
#include<iostream>
using namespace std;
class Class2;
class Class1
{
    int data1;
public:
    void setValue(int val1);
    void display();
    friend void exchange(Class1 &,Class2 &);
};
void Class1::setValue(int val1)
{
    data1=val1;
}
void Class1::display()
{
    cout<<"value1: "<<data1<<endl;
}
class Class2{
int data2;
public:
    void setValue(int val1);
    void display();
    friend void exchange(Class1 &,Class2 &);
};
void Class2::setValue(int val1)
{
    data2=val1;
}
void Class2::display()
{
    cout<<"value2: "<<data2<<endl;
}
void exchange(Class1 &a,Class2 &b)
{
    int temp;
    temp=a.data1;
    a.data1=b.data2;
    b.data2=temp;
}
int main()
{
    Class1 v1;
    Class2 v2;
    v1.setValue(5);
    v1.display();
    v2.setValue(10);
    v2.display();
    exchange(v1,v2);
    cout<<"After Swapping "<<endl;
    v1.display();
    v2.display();
    return 0;
}

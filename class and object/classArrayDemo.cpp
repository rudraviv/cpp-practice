//Shopping list program which demonstrate use of array in class
#include<iostream>
using namespace std;
const int size=15;
int temp=0;//used for variable array size
class Shopping
{
    string name[size];
    float price[size];
    int cnt=0;
public:
   void GetItem();
   void TotalBill();
   void DisplayList();
};

void Shopping::GetItem()
{

    cout<<"\nhow many item you will enter: ";
    cin>>temp;
    cout<<"\nEnter Item Name and Price"<<endl;
    for(int i=0;i<temp;i++)
    {
        cin>>name[i]>>price[i];
    }

}
void Shopping::DisplayList()
{
    cout<<"Item  "<<"Price"<<endl;
    for(int i=0;i<temp;i++)
    {
        cout<<name[i]<<"  "<<price[i]<<endl;
    }
}
void Shopping::TotalBill()
{
    float bill=0;
    for(int i=0;i<temp;i++)
    {
        bill+=price[i];
    }
    DisplayList();
    cout<<"======================"<<endl;
    cout<<"Total Bill  "<<bill<<"rs"<<endl;
}

int main()
{
    Shopping s;
    int ch;
    do
    {
        cout<<"\n1.Add item\n2.Display total bill\n3.display list\n4.exit\nEnter Choice: ";
        cin>>ch;
        switch(ch)
        {
        case 1:
            s.GetItem();
            break;
        case 2:
            s.TotalBill();
            break;
        case 3:
            s.DisplayList();
            break;
        case 4:
            break;
        }
    }while(ch!=4);
    return 0;
}

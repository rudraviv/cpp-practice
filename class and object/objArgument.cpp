//Time Addition implementation through objects as a argument
#include<iostream>
using namespace std;
class Time
{
private:
    int hours;
    int minutes;
public:
    void setTime(int ,int);
    void sum(Time,Time);        //objects are passed by value.
    void DisplayTime();
};
void Time::setTime(int hr,int mn)
{
    hours=hr;
    minutes=mn;
}
void Time::DisplayTime()
{
    cout<<hours<<" hour"<<minutes<<" minutes"<<endl;
}
void Time::sum(Time t1,Time t2)
{
minutes=t1.minutes+t2.minutes;
hours=minutes/60;
minutes=minutes%60;
hours=hours+t1.hours+t2.hours;
}
int main()
{
    Time t1,t2;
    t1.setTime(2,30);
    t2.setTime(1,45);
    Time t3;
    t3.sum(t1,t2);
    t1.DisplayTime();
    t2.DisplayTime();
    t3.DisplayTime();
    return 0;
}

#include<iostream>
using namespace std;
class items
{
    int number;     //These are instance variable.this variables will exist when object created.
    float cost;
public:
    void getdata(int a,float b);//defining outside class
    void putdata()
    {
        cout<<"number: "<<number<<endl;
        cout<<"cost: "<<cost<<endl;
    }
}x;
void items::getdata(int a,float b)
{
    number=a;
    cost=b;
}
int main()
{
    //items x;
    x.getdata(200,3.5);
    x.putdata();
    return 0;
}
/*
-Here we use getdata function to set values to instance variable.
-state of object is depend on values of instance variable.
-behavior of object means action performed by object through member functions.
-To avoid changing of state of object we make instance variable private,so that
 they are not directly accessible from main function.
-getdata is concept of getters and setters.
*/

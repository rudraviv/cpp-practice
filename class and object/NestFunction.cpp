//In this program we convert a binary number to it's one's complement.

#include<iostream>
#include<string>
#include<conio.h>

using namespace std;
class Binary
{
    string s;
public :
    void readBinary();
    void displayComplement();
    void convert();
    void check_binary();
};

void Binary::readBinary()
{
    cout<<"Enter a Binary Number: ";
    cin>>s;
    check_binary();
}

void Binary::check_binary()
{
    for(int i=0;i<s.length();i++)
    {
        if(s[i]!='0'&&s[i]!='1')
        {
            cout<<"\nEntered data is not a Binary Number"<<endl;
            cout<<"\nEnter any key to exit"<<endl;
            getch(); //Include conio.h file
            break;
        }
    }
}

void Binary::displayComplement()
{
    convert();
    cout<<"\n1's complement of binary number is : "<<s<<endl;
}
void Binary::convert()
{
    for(int i=0;i<s.length();i++)
    {
        if(s[i]=='1')
            s[i]='0';
        else
            s[i]='1';

    }
}
int main()
{
    Binary b;
    b.readBinary();
    b.displayComplement();
}

//Binary Operator Overloading
#include<iostream>
using namespace std;
class BinaryOverloading
{
    float real;
    float imag;
public :
    BinaryOverloading(float r=0,float i=0)//Default argument is for temp object
    {
        real=r,imag=i;
    }
    void display();
    BinaryOverloading operator+(BinaryOverloading);
};
void BinaryOverloading::display()
{
    cout<<real<<"+ j"<<imag<<endl;
}
BinaryOverloading BinaryOverloading::operator+(BinaryOverloading c)
{
    BinaryOverloading temp;
    temp.real=real+c.real;
    temp.imag=imag+c.imag;
    return temp;
}
int main()
{
    BinaryOverloading a,b,c;
    a=BinaryOverloading(2.5,3.5);
    b=BinaryOverloading(2.0,4.0);
    c=a.operator+(b);  //or c=a+b
    cout<<"C1=";
    a.display();
    cout<<"C2=";
    b.display();
    cout<<"C3=";
    c.display();
    return 0;
}

//Unary operator overloading
#include<iostream>
using namespace std;
class UnaryOverloading
{
    int x,y,z;
public :
    void getData(int a,int b,int c);
    void display();
    void operator-();
};

void UnaryOverloading::getData(int a,int b,int c)
{
    x=a;
    y=b;
    z=c;
}
void UnaryOverloading::display()
{
    cout<<"x= "<<x<<endl;
    cout<<"y= "<<y<<endl;
    cout<<"z= "<<z<<endl;
}
void UnaryOverloading::operator -()
{
    x=-x;
    y=-y;
    z=-z;
}

int main()
{
    UnaryOverloading op;
    op.getData(10,-20,30);
    cout<<"Before negation : "<<endl;
    op.display();
    -op;
    cout<<"After Negation: "<<endl;
    op.display();
    return 0;
}

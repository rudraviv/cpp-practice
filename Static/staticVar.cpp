#include<iostream>
using namespace std;

class Test
{
private:

public:
    static int objCount;
    Test()
    {
        ++objCount;
    }
};
int Test::objCount=0;       //this is necessary to initialize static variable in this way.
int main()
{
    Test a,b,c,d;
    cout<<"Total objects created: "<<Test::objCount;
    return 0;
}
/*
In this case we initialize static variable to 0 by using scope resolution operator but
when we declare static class variable as a private then it will give error if we initialize
it outside class as well as access it in main.
To avoid error we can use static class member function to deal with static class variables.
*/

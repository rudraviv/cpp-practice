#include<iostream>
using namespace std;
class Test
{
private:
    static int objCount;
public:
    Test()
    {
        ++objCount;
    }
    static void setCount(int a)
    {
        objCount=a;
    }
    static int getCount()
    {
        return objCount;
    }

};
int Test::objCount;     //This is part of syntax
int main()
{
    Test::setCount(0);
    //cout<<"size of class before object creation and after static initialization: "<<sizeof(Test)<<endl;

    Test a,c,v,b,f,d,s;
    cout<<"Total Objects Created: "<<Test::getCount();
    return 0;
}
/*
static class member function can access only static variables of class.
we can call these function without object creation.
*/

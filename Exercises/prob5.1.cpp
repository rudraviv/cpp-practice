//Bank Account
#include<iostream>
#include<string>
using namespace std;
class Bank
{
    string name;
    int AccNo;
    string Acctype;
    float balance;
public:
    Bank()
    {
        name="NULL";
        AccNo=0;
        Acctype="NULL";
        balance=0.0;
    }
    void init();
    void deposit();
    void withdraw();
    void AccDetails();
};
void Bank::init()
{
    cout<<"Enter Account Holder Name: "<<endl;
    cin>>name;
    cout<<"Enter Account Type:"<<endl;
    cin>>Acctype;
    cout<<"Enter Deposite Account opening amount(must be above 500)"<<endl;
    cin>>balance;
    if(balance>500)
    {
        cout<<"Account opened successfully";
        ++AccNo;
        cout<<"Account Number: "<<AccNo;
    }
    else
    {
        cout<<"You Entered amount less than 500....cant open account";

    }

}
void Bank::deposit()
{
    float amt;
     if(AccNo>0)
     {
         cout<<"Enter Amount:"<<endl;
         cin>>amt;
         balance+=amt;
         cout<<amt<<" deposited successfully"<<endl;
     }
     else
     {
         cout<<"open account first...."<<endl;
     }
}
void Bank::withdraw()
{
    float amt;
     if(AccNo>0)
     {
         cout<<"Enter Amount:"<<endl;
         cin>>amt;
         if(balance>amt)
         {
             balance-=amt;
             cout<<amt<<" deposited successfully"<<endl;
         }
         else
         {
             cout<<"Insufficient Balance"<<endl;
         }

     }
     else
     {
         cout<<"open account first...."<<endl;
     }
}
void Bank::AccDetails()
{
    if(AccNo!=0)
    {
     cout<<"Name: "<<name<<endl;
    cout<<"Account Type: "<<Acctype<<endl;
    cout<<"Account Number:"<<AccNo<<endl;
    cout<<"Balance: "<<balance<<endl;
    }
}
int main()
{
    Bank b;
    int ch;
    do
    {
        cout<<"**********MENU************";
        cout<<"\n0.Exit\n1.open account\n2.deposit\n3.withdraw\n4.Account Details";
        cout<<"\nEnter Choice: "<<endl;
        cin>>ch;
        switch(ch)
        {
        case 0:
            break;
        case 1:
            b.init();
            break;
        case 2:
            b.deposit();
            break;
        case 3:
            b.withdraw();
            break;
        case 4:
            b.AccDetails();
            break;
        default:
            cout<<"Please enter correct choice:";
        }
        cout<<endl;
    }while(ch!=0);
    return 0;

}

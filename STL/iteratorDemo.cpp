
#include<iostream>
#include<vector>
using namespace std;
int main()
{
    vector<int> v;
    for(int a=1;a<=3;a++)
    {
        v.push_back(a);
    }

    vector<int>::iterator i;

    cout<<"Without Iterator=";
    for(int j=0;j<3;j++)
    {
        cout<<v[j]<<" ";
    }

    cout<<"\nWith Iterator= ";

    for(i=v.begin();i<v.end();i++)
    {
        cout<<*i<<" ";
    }

    v.push_back(4);

    cout<<"\nWithout Iterator=";
    for(int j=0;j<4;j++)
    {
        cout<<v[j]<<" ";
    }

     cout<<"\nWith Iterator= ";

    for(i=v.begin();i<v.end();i++)
    {
        cout<<*i<<" ";
    }
    cout<<endl<<endl;
    return 0;
}
/*
As can be seen in the above code that without using iterators we need to keep
track of the total elements in the container. In the beginning there were only three elements,
but after one more element was inserted into it, accordingly the for loop also had to be amended,
but using iterators, both the time the for loop remained the same. So, iterator eased our task.
*/

#include<iostream>
#include<vector>
using namespace std;
int main()
{
    vector <int> arr={1,2,3,4,5};
    vector <int>::iterator ptr=arr.begin();

    advance(ptr,3);
    cout<<"iterator pointing to(using advance function): "<<*ptr<<endl;

    ptr=arr.begin();
    ptr=next(ptr,2);
    cout<<"iterator pointing to(using next function): "<<*ptr<<endl;
     ptr=prev(ptr,2);
    cout<<"iterator pointing to(using prev function): "<<*ptr<<endl;
}

//advance function increament iterator pointer by specified position.

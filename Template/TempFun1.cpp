//Program for swapping data using templates.
#include<iostream>
using namespace std;
template<class T> swapping(T a,T b)
{
    int temp;
    temp=a;
    a=b;
    b=temp;
cout<<"After Swapping a="<<a<<" b="<<b<<endl;
}

int main()
{

    swapping(2,3);
    swapping('P','Q');
    swapping(56.11,2.25);

    return 0;
}
